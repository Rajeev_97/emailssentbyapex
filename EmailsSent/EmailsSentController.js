/**
 * Created by Rajeev on 08-06-2020.
 */

({
    doInit: function (component, event) {
        component.set('v.mycolumns', [
            { label: 'ToAddress', fieldName: 'ToAddress', type: 'text' },
            { label: 'FromAddress', fieldName: 'FromAddress', type: 'text' },
            { label: 'Subject', fieldName: 'Subject', type: 'text' },
        ]);
        
    },

    onHandleDateChange: function (component, event) {
        if(event.detail.Name=='startDate'){
            component.set('v.startDate',event.detail.value);
        }
        else if(event.detail.Name=='EndDate'){
            component.set('v.endDate',event.detail.value);
        }
    },
    handleClick: function (component, event) {
        var action = component.get("c.getEmails");
        action.setParams({ startDate : cmp.get("v.startDate"), endDate : cmp.get("v.endDate")});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.expirationDatesList', response.getReturnValue());
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Success',
                    message: 'The Expirations Are Scheduled Successfully',
                    type: 'Success',

                });
                toastEvent.fire();

            }

            else if (state === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Error',
                    message: 'The Expirations Are Not Scheduled Successfully',
                    type: 'Error',

                });
                toastEvent.fire();
                console.log("Unknown error");
            }
        });
        $A.enqueueAction(action);
    },
   
})