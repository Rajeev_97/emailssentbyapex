/**
 * Created by Rajeev on 29-05-2020.
 */
// this class will call the Notification Schedular By caluculating the Dates from custome meta data
public with sharing class EmailController {

    public static List<EmailMessage> getEmailController(Date startDate,Date endDate) {
       
       String fields='select id, ccaddress, BccAddress, FromAddress, Subject, ToAddress,MessageDate,HtmlBody  from EmailMessage where MessageDate =:jobName';
       if(startDate != null  || endDate != null){
       String whereClause=' Where ';
       }
        if(startDate != null && startDate !=''){
           whereClause=whereClause+' MessageDate >=:startDate';
        }
        if(endDate != null && endDate !=''){
           whereClause=whereClause+' MessageDate <=:endDate';
        }
        String query=fields+whereClause;
        List<EmailMessage> listOfEmails=Datbase.query(query);
        if(!listOfEmails.isEmpty() ){
            return listOfEmails;
        }
        return null;
    }

}